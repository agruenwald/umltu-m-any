package at.tuvienna.umlTUJava.setups;

import org.eclipse.xtext.resource.generic.AbstractGenericResourceSupport;


import com.google.inject.Module;

public class APIGeneratorSupport extends
		AbstractGenericResourceSupport {
	@Override
	protected Module createGuiceModule() {
		return new APIGeneratorModule();
	}
}
