package at.tuvienna.umlTUJava.setups.daogen;

import org.eclipse.xtext.ISetup;


import com.google.inject.Guice;
import com.google.inject.Injector;

public class DAOGeneratorSetup implements ISetup {

	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		return Guice.createInjector(new DAOGeneratorModule());
	}

}
