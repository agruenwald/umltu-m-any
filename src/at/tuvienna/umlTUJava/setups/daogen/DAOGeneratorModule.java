package at.tuvienna.umlTUJava.setups.daogen;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule;
import at.tuvienna.umlTUJava.generators.*;

public class DAOGeneratorModule extends
		AbstractGenericResourceRuntimeModule {

	@Override
	protected String getLanguageName() {
		return "";
	}

	@Override
	protected String getFileExtensions() {
		return "umlTUJava";
	}

	public Class<? extends IGenerator> bindIGenerator() {
		return DAOGenerator.class;
	}

	public Class<? extends ResourceSet> bindResourceSet() {
		return ResourceSetImpl.class;
	}

}
