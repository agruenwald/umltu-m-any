package at.tuvienna.umlTUJava.setups;

import org.eclipse.xtext.ISetup;


import com.google.inject.Guice;
import com.google.inject.Injector;

public class APIGeneratorSetup implements ISetup {

	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		return Guice.createInjector(new APIGeneratorModule());
	}

}
