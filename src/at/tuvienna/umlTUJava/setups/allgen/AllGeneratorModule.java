package at.tuvienna.umlTUJava.setups.allgen;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule;
import at.tuvienna.umlTUJava.generators.*;

public class AllGeneratorModule extends
		AbstractGenericResourceRuntimeModule {

	@Override
	protected String getLanguageName() {
		return "";
	}

	@Override
	protected String getFileExtensions() {
		return "umlTUJava";
	}

	public Class<? extends IGenerator> bindIGenerator() {
		return AllGenerator.class;
	}

	public Class<? extends ResourceSet> bindResourceSet() {
		return ResourceSetImpl.class;
	}

}
