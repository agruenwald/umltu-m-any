package at.tuvienna.umlTUJava.setups.allgen;

import org.eclipse.xtext.ISetup;


import com.google.inject.Guice;
import com.google.inject.Injector;

public class AllGeneratorSetup implements ISetup {

	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		return Guice.createInjector(new AllGeneratorModule());
	}

}
