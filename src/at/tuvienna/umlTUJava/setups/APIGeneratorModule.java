package at.tuvienna.umlTUJava.setups;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule;

import at.tuvienna.umlTUJava.main.UmlTUJavaAPIGenerator;

public class APIGeneratorModule extends
		AbstractGenericResourceRuntimeModule {

	@Override
	protected String getLanguageName() {
		return "";//"at.ac.tuwien.big.me12.csv.csvmm.CSVPackage";
	}

	@Override
	protected String getFileExtensions() {
		return "umlTUJava";
	}

	public Class<? extends IGenerator> bindIGenerator() {
		return UmlTUJavaAPIGenerator.class;
	}

	public Class<? extends ResourceSet> bindResourceSet() {
		return ResourceSetImpl.class;
	}

}
