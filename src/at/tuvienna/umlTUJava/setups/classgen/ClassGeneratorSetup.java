package at.tuvienna.umlTUJava.setups.classgen;

import org.eclipse.xtext.ISetup;


import com.google.inject.Guice;
import com.google.inject.Injector;

public class ClassGeneratorSetup implements ISetup {

	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		return Guice.createInjector(new ClassGeneratorModule());
	}

}
