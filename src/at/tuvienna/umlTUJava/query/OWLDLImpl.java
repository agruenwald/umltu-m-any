package at.tuvienna.umlTUJava.query;


//import org.semanticweb.HermiT.Reasoner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxEditorParser;
import org.semanticweb.owlapi.expression.OWLEntityChecker;
import org.semanticweb.owlapi.expression.ParserException;
import org.semanticweb.owlapi.expression.ShortFormEntityChecker;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.util.BidirectionalShortFormProvider;
import org.semanticweb.owlapi.util.BidirectionalShortFormProviderAdapter;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;

import at.tuvienna.umlTUJava.ReasonerInterface;
import at.tuvienna.umlTUJava.UMLtuException;
//import org.semanticweb.HermiT.Reasoner;

/** 
 * Author: Matthew Horridge, The University of Manchester, Bio-Health Informatics Group
 * 		   and Andreas Gruenwald, a.gruenw@gmail.com
 * The QueryDL interface for the ontology and b) an interactive interface for browsing the ontology.
 * Only very basic implementation at the moment.
 **/
public class OWLDLImpl implements QueryInterface {
	public final static String INDIVIDUALS = "individuals";
	public final static String EQUIVALENT_CLASSES = "equivalent";
	public final static String SUPERCLASSES = "superclasses";
	public final static String SUBCLASSES = "subclasses";
	
	public List<List<String>> query(String query) throws UMLtuException {
		return this.query(query, INDIVIDUALS);
	}

	public List<List<String>>  queryIndividuals(String query) throws UMLtuException {
		return this.query(query, INDIVIDUALS);
	}
	
	public List<List<String>> query(String query, String type) throws UMLtuException {
		 try {
			 ReasonerInterface reasonerInterface = new ReasonerInterface();
			 try {
				 reasonerInterface.createDataFactory();
			 } catch (OWLOntologyStorageException e) {
				 e.printStackTrace();
				 throw new UMLtuException(e.getMessage());
			 }
			 OWLReasoner reasoner = null;
			 try {
				reasoner = reasonerInterface.createReasoner();
			} catch (OWLOntologyStorageException e) {
				throw new UMLtuException(e.getMessage());
			}
            ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
            DLQueryEngine dlQueryEngine = new DLQueryEngine(reasoner, shortFormProvider);
	        DLQueryPrinter dlQueryPrinter = new DLQueryPrinter(dlQueryEngine, shortFormProvider);
	        dlQueryPrinter.askQuery(query.trim(), false);
	        
	        List<List<String>> returnList = new LinkedList<List<String>>();
	        if(type.equalsIgnoreCase(SUPERCLASSES)) {
	        	Set<OWLClass> superClasses = dlQueryEngine.getSuperClasses(query, true);
	        	Iterator<OWLClass> it = superClasses.iterator();
	        	while(it.hasNext()) {
	        		OWLClass ind = it.next();
	        		LinkedList<String> l = new LinkedList<String>();
	        		l.add(shortFormProvider.getShortForm(ind));
	        		returnList.add(l);
	        	}
	        } else if(type.equalsIgnoreCase(EQUIVALENT_CLASSES)) {
	        	Set<OWLClass> equivalentClasses = dlQueryEngine.getEquivalentClasses(query);
	        	Iterator<OWLClass> it = equivalentClasses.iterator();
	        	while(it.hasNext()) {
	        		OWLClass ind = it.next();
	        		LinkedList<String> l = new LinkedList<String>();
	        		l.add(shortFormProvider.getShortForm(ind));
	        		returnList.add(l);
	        	}
	        } else if(type.equalsIgnoreCase(SUBCLASSES)) {
	        	Set<OWLClass> subClasses = dlQueryEngine.getSubClasses(query,true);
	        	Iterator<OWLClass> it = subClasses.iterator();
	        	while(it.hasNext()) {
	        		OWLClass ind = it.next();
	        		LinkedList<String> l = new LinkedList<String>();
	        		l.add(shortFormProvider.getShortForm(ind));
	        		returnList.add(l);
	        	}
	        } else {
	        	Set<OWLNamedIndividual> individuals = dlQueryEngine.getInstances(query, true);
	        	Iterator<OWLNamedIndividual> it = individuals.iterator();
	        	while(it.hasNext()) {
	        		OWLNamedIndividual ind = it.next();
	        		LinkedList<String> l = new LinkedList<String>();
	        		l.add(shortFormProvider.getShortForm(ind));
	        		returnList.add(l);
	        	}
	        }
	        return returnList;
	        
	        } catch (OWLOntologyCreationException e) {
	           throw new UMLtuException("Could not load ontology: " + e.getMessage());
	        } catch (ParserException e2) {
	        	throw new UMLtuException(String.format("Couldn't parse the query input: %s. (%s)",query,e2.getMessage()));
	        }
	}
	
	public void test(String query) throws UMLtuException {
		int i = 1;
		for(List<String> l : this.query(query)) {
			System.out.println("Individual Record " + i +": " + l.get(0));
			i++;
		}
	}
	
    public static void main(String[] args) throws UMLtuException, OWLOntologyStorageException {
        try {
        	ReasonerInterface reasonerInterface = new ReasonerInterface();
        	try {
				reasonerInterface.createDataFactory();
			} catch (OWLOntologyStorageException e) {
				e.printStackTrace();
				throw new UMLtuException(e.getMessage());
			}
        	
        	OWLReasoner reasoner = reasonerInterface.createReasoner();
        	
            // Load an example ontology. In this case, we'll just load the pizza
            // ontology.
            /*
        	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
            OWLOntology ontology = manager.loadOntologyFromOntologyDocument(IRI.create("file:"+UMLtuOWLLoader.ONTOLOGY_URI_INDIVIDUALS));
            System.out.println("Loaded ontology: " + ontology.getOntologyID());
            // We need a reasoner to do our query answering
            OWLReasoner reasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);//new Reasoner(ontology);//createReasoner(ontology);
            */
            
            // Entities are named using IRIs. These are usually too long for use
            // in user interfaces. To solve this
            // problem, and so a query can be written using short class,
            // property, individual names we use a short form
            // provider. In this case, we'll just use a simple short form
            // provider that generates short froms from IRI
            // fragments.
            ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
            // Create the DLQueryPrinter helper class. This will manage the
            // parsing of input and printing of results
            DLQueryPrinter dlQueryPrinter = new DLQueryPrinter(new DLQueryEngine(
                    reasoner, shortFormProvider), shortFormProvider);
            // Enter the query loop. A user is expected to enter class
            // expression on the command line.
            doQueryLoop(dlQueryPrinter);
        } catch (OWLOntologyCreationException e) {
            System.out.println("Could not load ontology: " + e.getMessage());
        } catch (IOException ioEx) {
            System.out.println(ioEx.getMessage());
        }
    }

    private static void doQueryLoop(DLQueryPrinter dlQueryPrinter) throws IOException {
        while (true) {
            // Prompt the user to enter a class expression
            System.out
            .println("Please type a class expression in Manchester Syntax and press Enter (or press x to exit):");
            System.out.println("");
            String classExpression = readInput();
            // Check for exit condition
            if (classExpression.equalsIgnoreCase("x")) {
                break;
            }
            dlQueryPrinter.askQuery(classExpression.trim(),true);
            System.out.println();
            System.out.println();
        }
    }

    private static String readInput() throws IOException {
        InputStream is = System.in;
        InputStreamReader reader;
        reader = new InputStreamReader(is, "UTF-8");
        BufferedReader br = new BufferedReader(reader);
        return br.readLine();
    }

    /*
    private static OWLReasoner createReasoner(OWLOntology rootOntology) {
        // We need to create an instance of OWLReasoner. An OWLReasoner provides
        // the basic query functionality that we need, for example the ability
        // obtain the subclasses of a class etc. To do this we use a reasoner
        // factory.
        // Create a reasoner factory.
        OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
        return reasonerFactory.createReasoner(rootOntology);
    }
    */
}

/** This example shows how to perform a "dlquery". The DLQuery view/tab in
 * Protege 4 works like this. */
class DLQueryEngine {
    private OWLReasoner reasoner;
    private DLQueryParser parser;

    /** Constructs a DLQueryEngine. This will answer "DL queries" using the
     * specified reasoner. A short form provider specifies how entities are
     * rendered.
     * 
     * @param reasoner
     *            The reasoner to be used for answering the queries.
     * @param shortFormProvider
     *            A short form provider. */
    public DLQueryEngine(OWLReasoner reasoner, ShortFormProvider shortFormProvider) {
        this.reasoner = reasoner;
        OWLOntology rootOntology = reasoner.getRootOntology();
        parser = new DLQueryParser(rootOntology, shortFormProvider);
    }

    /** Gets the superclasses of a class expression parsed from a string.
     * 
     * @param classExpressionString
     *            The string from which the class expression will be parsed.
     * @param direct
     *            Specifies whether direct superclasses should be returned or
     *            not.
     * @return The superclasses of the specified class expression
     * @throws ParserException
     *             If there was a problem parsing the class expression. */
    public Set<OWLClass> getSuperClasses(String classExpressionString, boolean direct)
            throws ParserException {
        if (classExpressionString.trim().length() == 0) {
            return Collections.emptySet();
        }
        OWLClassExpression classExpression = parser
                .parseClassExpression(classExpressionString);
        NodeSet<OWLClass> superClasses = reasoner
                .getSuperClasses(classExpression, direct);
        return superClasses.getFlattened();
    }

    /** Gets the equivalent classes of a class expression parsed from a string.
     * 
     * @param classExpressionString
     *            The string from which the class expression will be parsed.
     * @return The equivalent classes of the specified class expression
     * @throws ParserException
     *             If there was a problem parsing the class expression. */
    public Set<OWLClass> getEquivalentClasses(String classExpressionString)
            throws ParserException {
        if (classExpressionString.trim().length() == 0) {
            return Collections.emptySet();
        }
        OWLClassExpression classExpression = parser
                .parseClassExpression(classExpressionString);
        Node<OWLClass> equivalentClasses = reasoner.getEquivalentClasses(classExpression);
        Set<OWLClass> result;
        if (classExpression.isAnonymous()) {
            result = equivalentClasses.getEntities();
        } else {
            result = equivalentClasses.getEntitiesMinus(classExpression.asOWLClass());
        }
        return result;
    }

    /** Gets the subclasses of a class expression parsed from a string.
     * 
     * @param classExpressionString
     *            The string from which the class expression will be parsed.
     * @param direct
     *            Specifies whether direct subclasses should be returned or not.
     * @return The subclasses of the specified class expression
     * @throws ParserException
     *             If there was a problem parsing the class expression. */
    public Set<OWLClass> getSubClasses(String classExpressionString, boolean direct)
            throws ParserException {
        if (classExpressionString.trim().length() == 0) {
            return Collections.emptySet();
        }
        OWLClassExpression classExpression = parser
                .parseClassExpression(classExpressionString);
        NodeSet<OWLClass> subClasses = reasoner.getSubClasses(classExpression, direct);
        return subClasses.getFlattened();
    }

    /** Gets the instances of a class expression parsed from a string.
     * 
     * @param classExpressionString
     *            The string from which the class expression will be parsed.
     * @param direct
     *            Specifies whether direct instances should be returned or not.
     * @return The instances of the specified class expression
     * @throws ParserException
     *             If there was a problem parsing the class expression. */
    public Set<OWLNamedIndividual> getInstances(String classExpressionString,
            boolean direct) throws ParserException {
        if (classExpressionString.trim().length() == 0) {
            return Collections.emptySet();
        }
        OWLClassExpression classExpression = parser
                .parseClassExpression(classExpressionString);
        NodeSet<OWLNamedIndividual> individuals = reasoner.getInstances(classExpression,
                direct);
        return individuals.getFlattened();
    }
}

class DLQueryParser {
    private OWLOntology rootOntology;
    private BidirectionalShortFormProvider bidiShortFormProvider;

    /** Constructs a DLQueryParser using the specified ontology and short form
     * provider to map entity IRIs to short names.
     * 
     * @param rootOntology
     *            The root ontology. This essentially provides the domain
     *            vocabulary for the query.
     * @param shortFormProvider
     *            A short form provider to be used for mapping back and forth
     *            between entities and their short names (renderings). */
    public DLQueryParser(OWLOntology rootOntology, ShortFormProvider shortFormProvider) {
        this.rootOntology = rootOntology;
        OWLOntologyManager manager = rootOntology.getOWLOntologyManager();
        Set<OWLOntology> importsClosure = rootOntology.getImportsClosure();
        // Create a bidirectional short form provider to do the actual mapping.
        // It will generate names using the input
        // short form provider.
        bidiShortFormProvider = new BidirectionalShortFormProviderAdapter(manager,
                importsClosure, shortFormProvider);
    }

    /** Parses a class expression string to obtain a class expression.
     * 
     * @param classExpressionString
     *            The class expression string
     * @return The corresponding class expression
     * @throws ParserException
     *             if the class expression string is malformed or contains
     *             unknown entity names. */
    public OWLClassExpression parseClassExpression(String classExpressionString)
            throws ParserException {
        OWLDataFactory dataFactory = rootOntology.getOWLOntologyManager()
                .getOWLDataFactory();
        // Set up the real parser
        ManchesterOWLSyntaxEditorParser parser = new ManchesterOWLSyntaxEditorParser(
                dataFactory, classExpressionString);
        parser.setDefaultOntology(rootOntology);
        // Specify an entity checker that wil be used to check a class
        // expression contains the correct names.
        OWLEntityChecker entityChecker = new ShortFormEntityChecker(bidiShortFormProvider);
        parser.setOWLEntityChecker(entityChecker);
        // Do the actual parsing
        return parser.parseClassExpression();
    }
}

class DLQueryPrinter {
    private DLQueryEngine dlQueryEngine;
    private ShortFormProvider shortFormProvider;

    /** @param engine
     *            the engine
     * @param shortFormProvider
     *            the short form provider */
    public DLQueryPrinter(DLQueryEngine engine, ShortFormProvider shortFormProvider) {
        this.shortFormProvider = shortFormProvider;
        dlQueryEngine = engine;
    }

    /** @param classExpression
     *            the class expression to use for interrogation */
    public void askQuery(String classExpression, boolean printOn) {
        if (classExpression.length() == 0) {
            System.out.println("No class expression specified");
        } else {
            try {
                StringBuilder sb = new StringBuilder();
                if(printOn) {
	                sb.append("\n--------------------------------------------------------------------------------\n");
	                sb.append("QUERY:   ");
	                sb.append(classExpression);
	                sb.append("\n");
	                sb.append("--------------------------------------------------------------------------------\n\n");
                }
	             // Ask for the subclasses, superclasses etc. of the specified
                // class expression. Print out the results.
                Set<OWLClass> superClasses = dlQueryEngine.getSuperClasses(
                        classExpression, true);
                if (printOn) {
                	printEntities("SuperClasses", superClasses, sb);
                }
                
                Set<OWLClass> equivalentClasses = dlQueryEngine
                        .getEquivalentClasses(classExpression);
                if(printOn) {
                	printEntities("EquivalentClasses", equivalentClasses, sb);
                }
                Set<OWLClass> subClasses = dlQueryEngine.getSubClasses(classExpression,
                        true);
                if(printOn) {
                	printEntities("SubClasses", subClasses, sb);
                }
                Set<OWLNamedIndividual> individuals = dlQueryEngine.getInstances(
                        classExpression, true);
                if(printOn) {
                	printEntities("Instances", individuals, sb);
                	System.out.println(sb.toString());
                }
            } catch (ParserException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void printEntities(String name, Set<? extends OWLEntity> entities,
            StringBuilder sb) {
        sb.append(name);
        int length = 50 - name.length();
        for (int i = 0; i < length; i++) {
            sb.append(".");
        }
        sb.append("\n\n");
        if (!entities.isEmpty()) {
            for (OWLEntity entity : entities) {
                sb.append("\t");
                sb.append(shortFormProvider.getShortForm(entity));
                sb.append("\n");
            }
        } else {
            sb.append("\t[NONE]\n");
        }
        sb.append("\n");
    }
}
