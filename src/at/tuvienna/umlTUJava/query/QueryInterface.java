package at.tuvienna.umlTUJava.query;

import java.util.List;

import at.tuvienna.umlTUJava.UMLtuException;

/**
 * @author andreas_gruenwald
 * This interface facilitates the queries of varies different query languages, such as 
 * OWL-DL, SPARQL or SPARQL-DL over one interface.
 */
public interface QueryInterface {
	public List<List<String>> query(String query) throws UMLtuException;
	public void test (String query) throws UMLtuException;
}
