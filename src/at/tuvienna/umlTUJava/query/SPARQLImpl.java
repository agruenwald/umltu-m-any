package at.tuvienna.umlTUJava.query;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.mindswap.pellet.KnowledgeBase;
import org.mindswap.pellet.jena.PelletInfGraph;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import at.tuvienna.umlTUJava.ReasonerInterface;
import at.tuvienna.umlTUJava.UMLtuException;
import at.tuvienna.umlTUJava.UMLtuOWLLoader;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;

/**
 * @author andreas_gruenwald (a.gruenw@gmail.com)
 * This class represents the standard access to the OWL ontology via SPARQL.
 * Jena is wrapped onto the OWLAPI to support SPARQL access.
 */
public class SPARQLImpl implements QueryInterface {
	private final static Logger logger = Logger.getLogger(SPARQLImpl.class);
	
	protected String addPrefix(String query) {
		String prefix = "PREFIX itpm: <"+UMLtuOWLLoader.PMBOK_ONTOLOGY_IRI+"#" + "> ";
		return prefix + " " + query;
	}
	
	/* example: http://clarkparsia.com/pellet/faq/owlapi-sparql/ */
	public List<List<String>> query(String sparqlQuery) throws UMLtuException {
		logger.debug(String.format("The SPARQL query: %s", sparqlQuery));
		ResultSet results = this.queryInternal(sparqlQuery);
		return this.convertResultSet(results);
	}
	
	public void test(String sparqlQuery) throws UMLtuException {
		logger.info(String.format("The SPARQL query: %s", sparqlQuery));
		ResultSet results = this.queryInternal(sparqlQuery);
		ResultSetFormatter.out(System.out, results) ;
	}
	
	/* example: http://clarkparsia.com/pellet/faq/owlapi-sparql/ */
	protected ResultSet queryInternal(String sparqlQuery) throws UMLtuException {
		sparqlQuery = addPrefix(sparqlQuery);
		
		ReasonerInterface ri = new ReasonerInterface();
		 try {
			 ri.createDataFactory();
		 } catch (OWLOntologyStorageException | OWLOntologyCreationException e) {
			 e.printStackTrace();
			 throw new UMLtuException(e.getMessage());
		 }
		
		PelletReasoner owlapiReasoner = null;
		try {
			owlapiReasoner = ri.createNonBufferingReasoner(); // (non-buffering mode makes synchronization easier)
		} catch (OWLOntologyStorageException e) {
			e.printStackTrace();
			throw new UMLtuException(e.getMessage());
		} 
		KnowledgeBase kb = owlapiReasoner.getKB(); // Get the KB from the reasoner
		
		PelletInfGraph graph = new org.mindswap.pellet.jena.PelletReasoner().bind(kb); // Create a Pellet graph using the KB from OWLAPI
		InfModel model = ModelFactory.createInfModel(graph); // Wrap the graph in a model
		QueryExecution qExec = QueryExecutionFactory.create(sparqlQuery, model);	
		ResultSet results = qExec.execSelect();
		return results;
	}
	
	protected List<List<String>> convertResultSet(ResultSet results) {
		List<List<String>> resultList = new LinkedList<List<String>>();
		while(results.hasNext()) {
			QuerySolution solution = results.next();
			Iterator<String> it = solution.varNames();
			List<String> row = new LinkedList<String>();
			while(it.hasNext()) {
				String v = it.next();
				String out = solution.get(v).toString();
				if(out.length()>0 && out.contains("^^")) {
					row.add(out.split("\\^\\^")[0]);
				} else {
					//at the moment do nothing (links to other entries?)
					//row.add("NOT_FOUND.");
				}
			}
			resultList.add(row);
		}
		return resultList;
	}
}
