package at.tuvienna.umlTUJava.query;

import java.util.List;

import org.mindswap.pellet.KnowledgeBase;
import org.mindswap.pellet.jena.PelletInfGraph;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import at.tuvienna.umlTUJava.ReasonerInterface;
import at.tuvienna.umlTUJava.UMLtuException;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.sparqldl.jena.SparqlDLExecutionFactory;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class SPARQLDLImpl extends SPARQLImpl implements QueryInterface {

	
	/* example: http://lists.owldl.com/pipermail/pellet-users/2008-December/003215.html */
	@Override
	public List<List<String>> query(String sparqlQuery) throws UMLtuException {
		sparqlQuery = addPrefix(sparqlQuery);
		
		ReasonerInterface ri = new ReasonerInterface();
		 try {
			 ri.createDataFactory();
		 } catch (OWLOntologyStorageException | OWLOntologyCreationException e) {
			 e.printStackTrace();
			 throw new UMLtuException(e.getMessage());
		 }
	
		PelletReasoner owlapiReasoner = null;
		try {
			owlapiReasoner = ri.createReasoner(); 
		} catch (OWLOntologyStorageException e) {
			e.printStackTrace();
			throw new UMLtuException(e.getMessage());
		} 
		KnowledgeBase kb = owlapiReasoner.getKB(); // Get the KB from the reasoner
		// Create Pellet-Jena reasoner
		org.mindswap.pellet.jena.PelletReasoner jenaReasoner = new org.mindswap.pellet.jena.PelletReasoner();
		PelletInfGraph graph = jenaReasoner.bind(kb);
		// Wrap the graph in a model
		Model model2 = ModelFactory.createInfModel(graph);
		// Create a query execution over this model
		Query queryx = QueryFactory.create(sparqlQuery);
		QueryExecution qExec = SparqlDLExecutionFactory.create(queryx, model2);
				
		//run it
		ResultSet results = qExec.execSelect();
		return this.convertResultSet(results);
	}
}
