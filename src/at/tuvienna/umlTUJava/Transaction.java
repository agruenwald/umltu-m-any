package at.tuvienna.umlTUJava;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

public class Transaction {
	private final static Logger logger = Logger.getLogger(Transaction.class);
	
	private ReasonerInterface reasonerInterface = new ReasonerInterface();
	private OWLDataFactory factory = null;
	protected boolean autocommit;
	protected boolean autocommitPrevious;
	
	public Transaction() {
		this.autocommit = true;
		this.autocommitPrevious = true;
	}
	
	public Transaction(boolean autocommit) {
		this.autocommit = autocommit;
		this.autocommitPrevious = autocommit;
	}
	
	public void autoBegin() throws UMLtuException {
		if(autocommit) {
			reasonerInterface = new ReasonerInterface();
			try {
				factory = reasonerInterface.createDataFactory();
			} catch (OWLOntologyCreationException | OWLOntologyStorageException e) {
				logger.error(e.getMessage());
				throw new UMLtuException(e.getMessage());
			}
			
			//Set<OWLOntology> importsClosure = reasoner.getManager().getImportsClosure(reasoner.getOntology());
			/*
			//reasoner.getManager().
			reasoner.getManager().
            reasoner.loadOntologies(importsClosure);            
            reasoner.classify();
            */
		}
	}
	
	public void autoCommit() throws UMLtuException {
		if(autocommit) {
			try {
				reasonerInterface.inferAndSave();
		 	} catch (OWLOntologyStorageException e) {
		 		logger.error(e.getMessage());
		 		throw new UMLtuException(e.getMessage());
		 	}
		}
	}
	
	public void autoCommitReadOnly() {
		
	}
	
	public void disableAutocommit() {
		this.autocommitPrevious=autocommit;
		this.autocommit=false;
	}
	
	public void resetAutocommit() {
		this.autocommit=autocommitPrevious;
	}
	
	public OWLDataFactory getFactory() {
		return factory;
	}
	
	public ReasonerInterface getReasoner() {
		return reasonerInterface;
	}
}
