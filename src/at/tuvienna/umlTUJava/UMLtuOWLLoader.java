package at.tuvienna.umlTUJava;

import org.apache.log4j.Logger;

import at.tuvienna.main.Main;
import at.tuvienna.metamodel.MetaModel;
import at.tuvienna.utils.SettingsReader;

public class UMLtuOWLLoader {
	private final static Logger logger = Logger.getLogger(Main.class);
	private static MetaModel metaModel; 
	private static String ABSOLUTE_PATH = "/Users/andreas_gruenwald/da-agr/resources/design"; /** change here */ 
	public final static String ONTOLOGY_URI = ABSOLUTE_PATH + "/itpm.owl";
	public final static String ONTOLOGY_URI_INDIVIDUALS = ABSOLUTE_PATH + "/extendeditpm.owl";
	public final static String PMBOK_ONTOLOGY_IRI="http://www.tuwien.ac.at/itpm.owl";
	public final static String DOMAIN_ONTOLOGY_IRI="http://www.tuwien.ac.at/extendeditpm.owl";
	private final static String[] args = new String[] {
			"converter=VisualParadigmConverterXMI2",
			"IRI="+PMBOK_ONTOLOGY_IRI,
			"input="+ABSOLUTE_PATH+"/itpm.uml",
			"output="+ONTOLOGY_URI,
			"settings="+ABSOLUTE_PATH+"/settings-itpm.properties" 
		};
	
	public static void initializeSettings() {
		for(String a : args) {
			String[] s = a.split("=");
			SettingsReader.overwriteProperty(s[0], s[1]);
		}
	}
	/**
	 * @param args
	 */
	public static MetaModel loadMetaModel() {	
		try {
			if(metaModel!=null) {
				return metaModel;
			}
			
			//check input parameters and create meta model.
			metaModel = Main.loadMetaModel(args);
			if (metaModel == null) {
				return null;
			}
					
			//Harmonize, e.g. create IRI's.
			metaModel = Main.harmonizeModel(metaModel);		
			
			//Create OWL Ontology
			if (Main.createOWL(metaModel)) {
				logger.info(String.format("Finished transformation process (%s class elements contained in resulting ontology).",metaModel.getSize()));
			}
		} catch (Exception e) {
			logger.fatal(String.format("Transformation process failed. " +
							"Please make shure that you used the correct input format (e.g. Visual Paradigm XMI 2.1 export files and not the vpp - project files)." +
							"Exception: %s. If you have further problems, send a mail to <a.gruenw@gmail.com> and describe/attach your input file.",
					e.getMessage()));
			 e.printStackTrace();
			return metaModel;
		}
		return metaModel;
	}

}
