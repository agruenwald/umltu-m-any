package at.tuvienna.umlTUJava;

public class ManualTransaction extends Transaction {

	public ManualTransaction() {
		super(true);
	}
	
	public void begin() throws UMLtuException {
		super.autoBegin();
		super.disableAutocommit();
	}
	
	public void commit() throws UMLtuException {
		this.autocommitPrevious=this.autocommit;
		this.autocommit=true;
		super.autoCommit();
		this.autocommit=false; //TODO
	}
}
