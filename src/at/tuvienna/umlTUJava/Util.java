package at.tuvienna.umlTUJava;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Date;

public class Util {
	
	public static String checkMandatory(String s, String name) {
		if(s==null || s.isEmpty()) {
			return String.format("%s is empty.",name);
		} else if (s.length()<3) {
			return String.format("Length of string %s is smaller than 3.",name);
		} else {
			return "";
		}
	}
	
	public static String checkMandatory(Integer s, String name) {
		if(s==null) {
			return String.format("%s is empty.",name);
		} else if (s<=0) {
			return String.format("Integer %s must be greater than zero.",name);
		} else {
			return "";
		}
	}
	
	public static String checkMandatory(Double s, String name) {
		if(s==null) {
			return String.format("%s is empty.",name);
		} else if (s<=0) {
			return String.format("Double %s must be greater than zero.",name);
		} else {
			return "";
		}
	}
	
	public static String checkMandatory(boolean s, String name) {
		return "";
	}
	
	public static String checkMandatory(Date d, String name) {
		if(d==null) {
			return String.format("Date %s is empty.",name);
		} else {
			return "";
		}
	}
	
	public static String generateId(String s) {
		//TODO
		return s;
	}
	
	public static int generateId(int id) {
		//TODO
		return id;
	}
	
	public static Date parseDateTime(String literal) throws UMLtuException{
		try {
			String date = literal.split("T")[0];
			return new java.text.SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (Exception e) {
			throw new UMLtuException(e.getMessage());
		}
	}
	
	public static void copyFile(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}
	
	/**
	 * @param path a valid absolute file path, e.g. /Volumes/a/x.txt
	 * @return the file path without the name of the file at the end (e.g. /Volumes/a/)
	 */
	public static String getFilePathWithoutFileName(String path) {
		String filePath = path;
		String filePathReverse = new StringBuffer(filePath).reverse().toString();
		int slashPos = filePathReverse.length() - filePathReverse.indexOf("/"); //last slash
		filePath = filePath.substring(0,slashPos);
		return filePath;
	}

}
