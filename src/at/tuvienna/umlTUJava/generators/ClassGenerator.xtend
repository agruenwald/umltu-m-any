package at.tuvienna.umlTUJava.generators

import org.eclipse.xtext.generator.IFileSystemAccess
import at.tuvienna.umlTUJava.generators.BasicGenerator
import at.tuvienna.metamodel.MetaClass
import at.tuvienna.metamodel.MetaPackage
import at.tuvienna.metamodel.MetaModel

class ClassGenerator extends BasicGenerator {

	def ClassGenerator() {
		
	}
	
	override boolean generateSpecifics(IFileSystemAccess fsa, MetaModel metaModel) {
		var it = metaModel.packages.iterator;
			while(it.hasNext) {
				var metaPackage = it.next
				var itClasses = metaPackage.classes.iterator
				while(itClasses.hasNext) {
					var clazz = itClasses.next
					generateClass(clazz,metaPackage,fsa)
				}
			}
		return true
	}
	
	def generateClass(MetaClass clazz, MetaPackage pkg, IFileSystemAccess fsa) {
		var packageName = pkg.name
		fsa.generateFile('''«packageName.pkgSlashed»«clazz.name».java''',this.generateClass(pkg, clazz))
	}
	
	def generateClass(MetaPackage pkg, MetaClass clazz) { 
			'''
			package «pkg.name»;
			«handleImports(clazz)»
			«IF clazz.hasList»
				import java.util.*;
			«ENDIF»
			
			/*
			 	Java Beans representing a link between the static UML
			 	class diagram structure and the flexible OWL ontology.
				Generated by umlTUowl plus XTEND2 Java template.
				author: Andreas Gruenwald (a.gruenw@gmail.com)
			*/
			«clazz.classTypeGeneration» implements java.io.Serializable {
				private static final long serialVersionUID = 1L;
				«FOR a : clazz.attributes»
					private «a.range.dataType» «a.name.toFirstLower»;
				«ENDFOR»
				
				«FOR ass : clazz.associations»
					«IF ass.maxRange == 1»
						private «ass.to.metaClass.name.toFirstUpper» «ass.name.toFirstLower»; //range «ass.to.range»
					«ELSE»
						private final List<«ass.to.metaClass.name.toFirstUpper»> «ass.name.toFirstLower» = new LinkedList<«ass.to.metaClass.name.toFirstUpper»>(); //range «ass.to.range»
					«ENDIF»
				«ENDFOR»
				
				«FOR a : clazz.attributes»
					public void set«a.name.toFirstUpper»(«a.range.dataType» «a.name.toFirstLower») {
						this.«a.name.toFirstLower»=«a.name.toFirstLower»;
					}
					
					public «a.range.dataType» get«a.name.toFirstUpper»() {
						return this.«a.name.toFirstLower»;
					}

				«ENDFOR»
				
				«IF clazz.selectAttributes.size<=0»
					private String id;
					«IF !clazz.enumClass»
						public void setId(String id) {
							this.id=id;
						}
						
						public String getId() {
							return this.id;
						}
					«ELSE»
						public String getId() {
							return "«clazz.name»";
						}
					«ENDIF»
				«ENDIF»
				«FOR ass : clazz.associations»
					«IF ass.maxRange == 1»
						public void set«ass.name.toFirstUpper»(«ass.to.metaClass.name.toFirstUpper» «ass.name.toFirstLower») {
							this.«ass.name.toFirstLower» = «ass.name.toFirstLower»;
						}
						
						public «ass.to.metaClass.name.toFirstUpper» get«ass.name.toFirstUpper»() {
							return «ass.name.toFirstLower»;
						}
					«ELSE»
					/*
						public void set«ass.name.toFirstUpper»(List<«ass.to.metaClass.name.toFirstUpper»> «ass.name.toFirstLower») {
							this.«ass.name.toFirstLower» = «ass.name.toFirstLower»;
						}
					*/	
					public List<«ass.to.metaClass.name.toFirstUpper»> get«ass.name.toFirstUpper»() {
						return this.«ass.name.toFirstLower»;
					}
					«ENDIF»
				«ENDFOR»
				@Override
				public String toString() {
					«IF clazz.selectAttributes.size<=0»
						return this.id;
					«ELSE»
						return String.valueOf(this.«clazz.detectId»);
					«ENDIF» 
				}
			}	
			'''
		 }
}