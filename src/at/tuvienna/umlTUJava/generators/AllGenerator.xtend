package at.tuvienna.umlTUJava.generators

import at.tuvienna.umlTUJava.generators.BasicGenerator
import at.tuvienna.metamodel.MetaModel
import org.eclipse.xtext.generator.IFileSystemAccess

class AllGenerator extends BasicGenerator {

	override boolean generateSpecifics(IFileSystemAccess fsa, MetaModel metaModel) {
		var generators = newArrayList(new ClassGenerator, new DAOGenerator, new JUnitGenerator)
		for(g : generators) {
			g.generateSpecifics(fsa,metaModel)
		}
		return true
	}
}