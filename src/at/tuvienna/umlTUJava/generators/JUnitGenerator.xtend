package at.tuvienna.umlTUJava.generators

import org.eclipse.xtext.generator.IFileSystemAccess
import at.tuvienna.metamodel.MetaClass
import at.tuvienna.metamodel.MetaPackage
import at.tuvienna.metamodel.MetaModel
import java.util.LinkedList

class JUnitGenerator extends BasicGenerator{
	
	def JUnitGenerator() {
		
	}
	
	override generateSpecifics(IFileSystemAccess fsa, MetaModel metaModel) {
		var it = metaModel.packages.iterator;
		while(it.hasNext) {
			var metaPackage = it.next
			var itClasses = metaPackage.classes.iterator
			while(itClasses.hasNext) {
				var clazz = itClasses.next
				generateUnitTests(clazz,metaPackage,fsa)
			}
		}
		return true
	}

	def handleImportsJunit(MetaClass clazz) {
		imported=false;
		importItpm=true
		'''
		«IF clazz.selectAssociations.size > 0»
			«FOR ass : clazz.selectAssociations»
				«IF importItpm»
					«ass.importAll»
				«ENDIF»
			«ENDFOR»
		«ENDIF»
		'''
	}	

	def generateTestData(String range, String name) {
		return range.generateTestData(name,"", 0)	
	}
	
	def generateTestData(String range, String name, String postfix, int postfixNum) {
		if(range.equals("String")) {
			var postfixI = "";
			if(postfixNum > 0) {
				postfixI = postfixNum.toString;
			}
			return "\"test" + postfix + postfixI +  "\"";
		} else  if (range.equals("boolean")) {
			return true;
		} else if (range.equals("DateTime")) {
			return "new java.util.GregorianCalendar(2013,10,10).getTime()";
		} else if (range.equals("Double")) {
			return 33.0 + try {Double::parseDouble(postfix); } catch (Exception e) {return 33.0 + postfixNum};
		} else {
			return 33 + try {Integer::parseInt(postfix); } catch (Exception e) {return 33 + postfixNum};
		}
	}

	
	def handleAssociationsJUnit(MetaClass clazz) {
		return handleAssociationsJUnit(clazz, "")
	}
	

	def handleAssociationsJUnit(MetaClass clazz, String x) {var rand = (Math::random*1000000).intValue.toString'''
		«FOR a : clazz.selectAssociations»«
			IF a.maxRange==1»
				«a.to.metaClass.firstConcreteClass.name.toFirstUpper» «a.to.metaClass.firstConcreteClass.name.toFirstLower»«rand» = new «a.to.metaClass.firstConcreteClass.name.toFirstUpper»();
				«FOR o : a.to.metaClass.selectAttributes»
				 	«a.to.metaClass.firstConcreteClass.name.toFirstLower»«rand».set«o.name.toFirstUpper»(«o.range.generateTestData(o.name, "xxx",(Math::random*10000).intValue)»);
				«ENDFOR»
				«clazz.name.toFirstLower»«x».set«a.name.toFirstUpper»(«a.to.metaClass.firstConcreteClass.name.toFirstLower»«rand»);«
			ELSE»
			List<«a.to.metaClass»> list«a.name»«rand» = «clazz.name.toFirstLower»«x».get«a.name.toFirstUpper»();
			for(int i=0; i < «a.minRange»; i++) {
				«a.to.metaClass.firstConcreteClass.name.toFirstUpper» «a.to.metaClass.firstConcreteClass.name.toFirstLower»«rand» = new «a.to.metaClass.firstConcreteClass.name.toFirstUpper»();
				«FOR o : a.to.metaClass.selectAttributes»
				 	«a.to.metaClass.firstConcreteClass.name.toFirstLower»«rand».set«o.name.toFirstUpper»(«o.range.generateTestData(o.name, "xxx",(Math::random*10000).intValue)»«IF o.range.equals("String")»+String.valueOf(i)«ELSEIF o.range.equalsIgnoreCase("int") || o.range.equalsIgnoreCase("Integer") »+«(Math::random*10000).intValue»+i«ENDIF»);
				«ENDFOR»
				list«a.name»«rand».add(«a.to.metaClass.firstConcreteClass.name.toFirstLower»«rand»);
			}
		«ENDIF»«ENDFOR»'''
	}
	
	def generateUnitTests(MetaClass clazz, MetaPackage pkg, IFileSystemAccess fsa) {
		var packageName = pkg.name
		if(!clazz.abstractClass && !clazz.enumClass) {
			fsa.generateFile('''«packageName+'.junit'.pkgSlashed»«'Test'+clazz.name».java''',this.generateUnitTest(pkg, clazz))
		}
	}
		 def generateUnitTest(MetaPackage pkg, MetaClass clazz) { 
		 	var instances = new LinkedList<Integer>();
		 	var i = 0
		 	while ((i=i+1) <= 10) {
		 		instances.add(i);
		 	}
			'''
			package «pkg.name+'.junit'»;
			import static org.junit.Assert.*;
			import java.util.*;
			import org.junit.Test;
			import org.junit.Before;
			import org.junit.After;
			import at.tuvienna.umlTUJava.*;
			import java.io.*;
			import itpm.«clazz.name»;
			import itpm.dao.DAO«clazz.name»;
			«clazz.handleImportsJunit»
			
			
			/*
			 	jUnit tests autogenerated by umlTUowl.
				author: Andreas Gruenwald (a.gruenw@gmail.com)
			*/
			public class «'Test'+clazz.name.toFirstUpper» {
					@Before
					//Store extendeditpm.owl in a temporary file.
					public void setup() throws IOException {
						File extendedITpm = new File(UMLtuOWLLoader.ONTOLOGY_URI_INDIVIDUALS);
						File tmpFile = new File(Util.getFilePathWithoutFileName(UMLtuOWLLoader.ONTOLOGY_URI_INDIVIDUALS) + "extended_tmp.owl");
						Util.copyFile(extendedITpm, tmpFile);
						UMLtuOWLLoader.initializeSettings();
					}
					
					@After
					//Restore extendeditpm.owl and remove temporary file after tests have been executed.
					public void tearDown() throws IOException {
						File extendedITpm = new File(UMLtuOWLLoader.ONTOLOGY_URI_INDIVIDUALS);
						File tmpFile = new File(Util.getFilePathWithoutFileName(UMLtuOWLLoader.ONTOLOGY_URI_INDIVIDUALS) + "extended_tmp.owl");
						Util.copyFile(tmpFile, extendedITpm);
						tmpFile.delete();
					}
			
				@Test
				public void testInsert() throws UMLtuException{
					«clazz.name» «clazz.name.toFirstLower» = new «clazz.name»();
					«FOR a : clazz.selectAttributes»
					 	«clazz.name.toFirstLower».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name)»);
					«ENDFOR»
					«clazz.handleAssociationsJUnit»
					DAO«clazz.name» dao = new DAO«clazz.name»();
					dao.insert(«clazz.name.toFirstLower»);
					«clazz.name» «clazz.name.toFirstLower»2 = dao.readRecord(«clazz.name.toFirstLower».get«clazz.selectAttributes.firstAttribute.name.toFirstUpper»());
					assertTrue(«clazz.name.toFirstLower»2 !=null);
					«FOR a : clazz.selectAttributes»
						assertEquals(«clazz.name.toFirstLower».get«a.name.toFirstUpper»(),
									 «clazz.name.toFirstLower»2.get«a.name.toFirstUpper»());
					«ENDFOR»
				}
				
				@Test
				public void testUpdate() throws UMLtuException {
					«clazz.name» «clazz.name.toFirstLower» = new «clazz.name»();
					«FOR a : clazz.selectAttributes»
					 	«clazz.name.toFirstLower».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name)»);
					«ENDFOR»
					«clazz.handleAssociationsJUnit»
					DAO«clazz.name» dao = new DAO«clazz.name»();
					dao.insert(«clazz.name.toFirstLower»);
					«FOR a : clazz.selectAttributes»
					 	«clazz.name.toFirstLower».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name,"Replaced",0)»);
					«ENDFOR»
					«clazz.name.toFirstLower».set«clazz.selectAttributes.firstAttribute.name.toFirstUpper»(«clazz.selectAttributes.firstAttribute.range.generateTestData(clazz.selectAttributes.firstAttribute.name,"",0)»);
					dao.update(«clazz.name.toFirstLower»);
					«clazz.name» «clazz.name.toFirstLower»2 = dao.readRecord(«clazz.name.toFirstLower».get«clazz.selectAttributes.firstAttribute.name.toFirstUpper»());
					assertTrue(«clazz.name.toFirstLower»2!=null);
					«FOR a : clazz.selectAttributes»
					
						«IF !a.name.equals(clazz.selectAttributes.firstAttribute.name) && a.range.equals("String")»
							assertEquals(«clazz.name.toFirstLower»2.get«a.name.toFirstUpper»(), "testReplaced");
						«ENDIF»
					«ENDFOR»
				}

				/**
					test if attributes of different instances are kept and not overwritten if two inserts take place one after another.
				**/
				@Test
				public void testInsertMultipleRecordsCheckAttributes() throws UMLtuException {;
					DAO«clazz.name» dao = new DAO«clazz.name»();
					List<«clazz.name»> list = new LinkedList<«clazz.name»>();
					«FOR x : newArrayList(1,2)  SEPARATOR "\n"»
						«clazz.name» «clazz.name.toFirstLower»«x» = new «clazz.name»();
						«FOR a : clazz.selectAttributes»
						 	«clazz.name.toFirstLower»«x».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name,x.toString,0)»);
						«ENDFOR»
						«clazz.handleAssociationsJUnit(x.toString)»
						dao.insert(«clazz.name.toFirstLower»«x»);
						list.add(«clazz.name.toFirstLower»«x»);
					«ENDFOR»
					
					for(int i = 0; i < list.size(); i++) {
						«clazz.name» entry = list.get(i);
						«clazz.name» «clazz.name.toFirstLower»3 = dao.readRecord(entry.get«clazz.selectAttributes.firstAttribute.name.toFirstUpper»());
						assertTrue(«clazz.name.toFirstLower»3!=null);
						«FOR a : clazz.selectAttributes»
							«IF !a.name.equals(clazz.selectAttributes.firstAttribute.name) && a.range.equals("String")»
								assertEquals(entry.get«a.name.toFirstUpper»(), «a.range.generateTestData(a.name,"",0)» + String.valueOf(i+1));
							«ENDIF»
						«ENDFOR»
					}
				}
				
				/**
					test if attributes of different instances are kept and not overwritten when updating instances
				**/
				@Test
				public void testUpdateMultipleRecordsCheckAttributes() throws UMLtuException {;
					DAO«clazz.name» dao = new DAO«clazz.name»();
					List<«clazz.name»> list = new LinkedList<«clazz.name»>();
					«FOR x : newArrayList(1,2)  SEPARATOR "\n"»
						«clazz.name» «clazz.name.toFirstLower»«x» = new «clazz.name»();
						«FOR a : clazz.selectAttributes»
						 	«clazz.name.toFirstLower»«x».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name,x.toString,0)»);
						«ENDFOR»
						«clazz.handleAssociationsJUnit(x.toString)»
						dao.insert(«clazz.name.toFirstLower»«x»);
						dao.update(«clazz.name.toFirstLower»«x»);
						list.add(«clazz.name.toFirstLower»«x»);
					«ENDFOR»
					
					for(int i = 0; i < list.size(); i++) {
						«clazz.name» entry = list.get(i);
						«clazz.name» «clazz.name.toFirstLower»3 = dao.readRecord(entry.get«clazz.selectAttributes.firstAttribute.name.toFirstUpper»());
						assertTrue(«clazz.name.toFirstLower»3!=null);
						«FOR a : clazz.selectAttributes»
							«IF !a.name.equals(clazz.selectAttributes.firstAttribute.name) && a.range.equals("String")»
								assertEquals(entry.get«a.name.toFirstUpper»(), «a.range.generateTestData(a.name,"",0)» + String.valueOf(i+1));
							«ENDIF»
						«ENDFOR»
					}
				}
				
				/**
					test if associations of different instances are kept and not overwritten when updating instances
				**/
				@Test
				public void testUpdateMultipleRecordsCheckAssociations() throws UMLtuException {;
					DAO«clazz.name» dao = new DAO«clazz.name»();
					List<«clazz.name»> list = new LinkedList<«clazz.name»>();
					«FOR x : newArrayList(1,2)  SEPARATOR "\n"»
						«clazz.name» «clazz.name.toFirstLower»«x» = new «clazz.name»();
						«FOR a : clazz.selectAttributes»
						 	«clazz.name.toFirstLower»«x».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name,x.toString,0)»);
						«ENDFOR»
						«clazz.handleAssociationsJUnit(x.toString)»
						dao.insert(«clazz.name.toFirstLower»«x»);
						dao.update(«clazz.name.toFirstLower»«x»);
						list.add(«clazz.name.toFirstLower»«x»);
					«ENDFOR»
					
					for(int i = 0; i < list.size(); i++) {
						«clazz.name» entry = list.get(i);
						«clazz.name» «clazz.name.toFirstLower»3 = dao.readRecord(entry.get«clazz.selectAttributes.firstAttribute.name.toFirstUpper»());
						assertTrue(«clazz.name.toFirstLower»3!=null);
						«FOR r : clazz.selectAssociations»
							«IF r.minRange==1 && r.maxRange==1»
								assertFalse(String.valueOf(«clazz.name.toFirstLower»3.get«r.name.toFirstUpper»().«r.to.metaClass.detectId»).equals(""));
							«ELSEIF r.minRange>=1»
								assertTrue(«clazz.name.toFirstLower»3.get«r.name.toFirstUpper»().size()>=«r.minRange»);
								for(«r.to.metaClass.name.toFirstUpper» x : «clazz.name.toFirstLower»3.get«r.name.toFirstUpper»()) {
									assertFalse(String.valueOf(x.«r.to.metaClass.detectId»).equals(""));
								}
							«ENDIF»
						«ENDFOR»
					}
				}
				
				@Test
				public void negTestDoubleInsert() throws UMLtuException {
					«clazz.name» «clazz.name.toFirstLower» = new «clazz.name»();
					«FOR a : clazz.selectAttributes»
					 	«clazz.name.toFirstLower».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name)»);
					«ENDFOR»
					«clazz.handleAssociationsJUnit»
					DAO«clazz.name» dao = new DAO«clazz.name»();
					dao.insert(«clazz.name.toFirstLower»);
					try {
						dao.insert(«clazz.name.toFirstLower»);
						assertTrue(false);
					} catch (UMLtuException e) {
						assertTrue(true);
					}
				}
				
				@Test
				public void testRemove() throws UMLtuException {
					«clazz.name» «clazz.name.toFirstLower» = new «clazz.name»();
					«FOR a : clazz.selectAttributes»
					 	«clazz.name.toFirstLower».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name)»);
					«ENDFOR»	
					«clazz.handleAssociationsJUnit»				
					DAO«clazz.name» dao = new DAO«clazz.name»();
					dao.insert(«clazz.name.toFirstLower»);
					«clazz.name» y = new «clazz.name»();
					y.set«clazz.selectAttributes.firstAttribute.name.toFirstUpper»(«clazz.selectAttributes.firstAttribute.range.generateTestData(clazz.selectAttributes.firstAttribute.name)»);
					dao.remove(y);
					«clazz.name.toFirstLower» = dao.readRecord(«clazz.name.toFirstLower».get«clazz.selectAttributes.firstAttribute.name.toFirstUpper»());
					assertTrue(«clazz.name.toFirstLower» == null);
				}

				@Test
				public void negTestRemove1() throws UMLtuException {
					«clazz.name» «clazz.name.toFirstLower»1 = new «clazz.name»();
					«clazz.name» «clazz.name.toFirstLower»2 = new «clazz.name»();
					«FOR a : clazz.selectAttributes»
					 	«clazz.name.toFirstLower»1.set«a.name.toFirstUpper»(«a.range.generateTestData(a.name,"1",0)»);
					 	«clazz.name.toFirstLower»2.set«a.name.toFirstUpper»(«a.range.generateTestData(a.name,"2",0)»);
					«ENDFOR»					
					«clazz.handleAssociationsJUnit("1")»
					«clazz.handleAssociationsJUnit("2")»
					DAO«clazz.name» dao = new DAO«clazz.name»();
					dao.insert(«clazz.name.toFirstLower»1);
					dao.insert(«clazz.name.toFirstLower»2);
					
					«clazz.name» y = new «clazz.name»();
					y.set«clazz.selectAttributes.firstAttribute.name.toFirstUpper»(«clazz.selectAttributes.firstAttribute.range.generateTestData(clazz.selectAttributes.firstAttribute.name,"1",0)»);
					dao.remove(y);
					y = dao.readRecord(«clazz.selectAttributes.firstAttribute.range.generateTestData(clazz.selectAttributes.firstAttribute.name,"2",0)»);
					assertTrue(y != null);
				}
				
				@Test
				public void testReadAll1() throws UMLtuException {
					DAO«clazz.name» dao = new DAO«clazz.name»();
					«clazz.name» «clazz.name.toFirstLower» = null;
					«FOR x : instances SEPARATOR "\n"»
						«clazz.name.toFirstLower» = new «clazz.name»();
						«FOR a : clazz.selectAttributes»
						 	«clazz.name.toFirstLower».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name,x.toString,0)»);
						«ENDFOR»
						«clazz.handleAssociationsJUnit»
						dao.insert(«clazz.name.toFirstLower»);
					«ENDFOR»
					List<«clazz.name»> list1 = dao.readFulltext("");
					List<«clazz.name»> list2 = dao.readFulltext(String.valueOf(«clazz.selectAttributes.firstAttribute.range.generateTestData(clazz.selectAttributes.firstAttribute.name,"3",0)»)); //3 because test1 finds test1 and test10 
					assertEquals(10,list1.size());
					assertEquals(1,list2.size());
				}
				
				@Test
				public void negTestReadAll1() throws UMLtuException {
					DAO«clazz.name» dao = new DAO«clazz.name»();
					«clazz.name» «clazz.name.toFirstLower» = null;
					«FOR x : instances  SEPARATOR "\n"»
						«clazz.name.toFirstLower» = new «clazz.name»();
						«FOR a : clazz.selectAttributes»
						 	«clazz.name.toFirstLower».set«a.name.toFirstUpper»(«a.range.generateTestData(a.name,x.toString,0)»);
						«ENDFOR»
						«clazz.handleAssociationsJUnit»
						dao.insert(«clazz.name.toFirstLower»);
					«ENDFOR»
					List<«clazz.name»> list = dao.readFulltext("xxxxxx"); 
					assertEquals(list.size(),0);
				}
			}	
			'''
		 }
}