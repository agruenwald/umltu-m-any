package at.tuvienna.umlTUJava.generators

import at.tuvienna.metamodel.MetaAssociation
import at.tuvienna.metamodel.MetaAttribute
import at.tuvienna.metamodel.MetaClass
import java.util.HashSet
import java.util.LinkedList
import java.util.List
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuvienna.umlTUJava.UMLtuOWLLoader
import at.tuvienna.metamodel.MetaModel

abstract class BasicGenerator implements IGenerator {

	def boolean generateSpecifics(IFileSystemAccess fsa, MetaModel m)
	
	def doLoadMetaModel(Resource resource, IFileSystemAccess fsa) {
			var metaModel = UMLtuOWLLoader::loadMetaModel();
			fsa.generateSpecifics(metaModel)
	}
	
	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		resource.doLoadMetaModel(fsa)
	}
	
	def getPkgSlashed(String packageName) {
		'''«packageName.replace('.','/')»/'''
	}
	
	
	def getFirstConcreteClass(MetaClass clazz) {
		if(clazz.abstractClass || clazz.interfaceClass) {
			for(c: clazz.subclasses) {
				if(!(c.firstConcreteClass.abstractClass || c.firstConcreteClass.interfaceClass)) {
					return c
				}
			}
		} else {
			return clazz
		}
	}
	
	def parseType(MetaAttribute attr, String variable) {
		'''«IF attr.range.equals("String")»«variable»«
			ELSEIF attr.range.equals("DateTime")»Util.parseDateTime(«variable»)«
			ELSEIF attr.range.equals("int") || attr.range.equals("Integer")»Integer.parseInt(«variable»)«
			ELSE»«attr.range.toFirstUpper».parse«attr.range.toFirstUpper»(«variable»)«ENDIF»'''
	}
	
	def detectId(MetaClass clazz) {
		'''get«clazz.detectIdAttribute»()'''
	}
	
	def detectIdAttribute(MetaClass clazz) {
		'''«IF clazz.selectAttributes.size()>0»«clazz.selectAttributes.get(0).name.toFirstUpper»«ELSE»Id«ENDIF»'''
	}
		 
	def getDataType(String range) {
		var dataTypes = new HashSet<String>();
		'''«IF range.equals("DateTime") && !dataTypes.contains("Date")»Date«
		  	ELSE»«range»«
		ENDIF»'''	
	}
	
	def castToGeneric(String range) {
		if(range.equalsIgnoreCase("string")) {
			return "String";
		} else if (range.equalsIgnoreCase("int") || range.equalsIgnoreCase("integer")) {
			return "Integer";
		} else {
			return range;
		}
	}
	
	def getIndividualAddress(MetaClass clazz) {
		'''"«clazz.indPrefix»"+«clazz.name.toFirstLower».«clazz.detectId»'''
	}
	
	def getIndividualAddressName(MetaClass clazz, String name) {
		'''"«clazz.indPrefix»"+«name».«clazz.detectId»'''
	}
	
	def getIndPrefix(MetaClass clazz) {
		'''«clazz.name»-'''
	}
	
	var classList = new LinkedList<MetaClass>();
	def selectAttributes(MetaClass clazz) {

		var List<MetaAttribute> list = new LinkedList<MetaAttribute>();
		classList = new LinkedList<MetaClass>()
		for(MetaClass c : clazz.selectClassesD.reverse) {
			list.addAll(c.attributes)
		}
		return list
	}
	
	def getFirstAttribute(List<MetaAttribute> att) {
		if(att.size>0) {
			return att.get(0)
		} else {
			var a = new MetaAttribute("id");
			a.setRange("String");
			return a;
		}
	}
	
	def selectClassesD(MetaClass clazz) {
		classList.add(clazz)
		for(MetaClass su : clazz.superclasses) {
			su.selectClassesD
		}
		return classList
	}
	
	def selectAssociations(MetaClass clazz) {
		var List<MetaAssociation> list = new LinkedList<MetaAssociation>();
		classList = new LinkedList<MetaClass>()
		for(MetaClass c : clazz.selectClassesD.reverse) {
			list.addAll(c.associations)
		}
		return list
	}
	
	
	def selectNonAbstractSubclasses(MetaClass clazz) {
		val classes = new LinkedList<MetaClass>();
		return clazz.selectSubclassesD(classes)
	}
	
	def selectSubclassesD(MetaClass clazz, LinkedList<MetaClass> classes) {
		if(!clazz.abstractClass && !clazz.interfaceClass) {
			classes.add(clazz)	
		}
		for(MetaClass c : clazz.subclasses) {
			c.selectSubclassesD(classes)
		}
		return classes
	}
	
	protected var imported=false;
	def handleImports(MetaClass clazz) {
		imported=false;
		'''
		«FOR a : clazz.attributes»
			«IF a.range.equals("DateTime") && !imported»
				«clazz.generateDateImport»
			«ENDIF»
		«ENDFOR»
		'''
	}
	

	protected var importItpm=false
	def importAll(MetaAssociation ass) { 
		importItpm=false
		'''import itpm.*;'''
	}
	

	
	def generateDateImport(MetaClass clazz){
		imported=true
		return "import java.util.Date;"
	}
	
	def classTypeGeneration(MetaClass clazz) {
		
		'''public «IF clazz.abstractClass»abstract «ENDIF»«
		    IF clazz.interfaceClass»interface «ELSE»class «ENDIF»«clazz.name»«
		     IF !clazz.superclasses.isEmpty() && clazz.interfaceClass» implements «FOR s : clazz.superclasses SEPARATOR ','»s.name«ENDFOR»«ENDIF»«
		     IF !clazz.superclasses.isEmpty()» extends «clazz.superclasses.iterator.next»«ENDIF»'''
	}
	
	
	def hasList(MetaClass clazz) {
		for(a : clazz.associations) {
			if(a.maxRange==-1) {
				return true
			}
		}
		return false
	}
	/**
	 * returns -1 for unlimited associations,
	 * otherwise the maximum value
	 */
	def getMaxRange(MetaAssociation ass) {
		var i = -1
		var unlimited = false
		for(r : ass.to.range) {
			if (r.matches("[0-9]+")) {
				var j=Integer::parseInt(r)
				if(j>i) {
					i=j
				}
			} else if (r.contains("*") || r.trim.isEmpty) {
				unlimited=true
			}
		}
		if(unlimited) {
			return -1
		} else {
				return i	
		}
	}
	
	/**
	 * returns -1 for unlimited associations,
	 * otherwise the maximum value
	 */
	def getMinRange(MetaAssociation ass) {
		var i = 0
		var start = true
		for(r : ass.to.range) {
			if (r.matches("[0-9]+")) {
				var j=Integer::parseInt(r);
				if(j<i || start==true) {
					i=j
					start=false
				}
			}
			if(r.trim.isEmpty){
				i=0
			}
		}
		return i
	}
}