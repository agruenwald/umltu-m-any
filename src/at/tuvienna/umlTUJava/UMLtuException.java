package at.tuvienna.umlTUJava;

import java.util.List;

public class UMLtuException extends Exception {
	private static final long serialVersionUID = 4564196422960311516L;
	
	public UMLtuException(String error) {
		super(error);
	}
	
	public UMLtuException(List<String> errors) {
		super(errors.get(0));
	}
}
