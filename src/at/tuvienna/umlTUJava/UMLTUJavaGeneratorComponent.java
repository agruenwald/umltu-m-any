package at.tuvienna.umlTUJava;

import org.eclipse.emf.mwe2.runtime.workflow.IWorkflowContext;
import org.eclipse.xtext.generator.GeneratorComponent;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;

public class UMLTUJavaGeneratorComponent extends GeneratorComponent {
	
	@Override
	public void preInvoke() {
		//input has to be checked manually
	}
	
	@Override
	public void invoke(IWorkflowContext ctx) {
		IGenerator instance = getCompiler();
		IFileSystemAccess fileSystemAccess = getConfiguredFileSystemAccess();
		instance.doGenerate(null, fileSystemAccess);
	}

	
}