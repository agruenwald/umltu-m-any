package at.tuvienna.umlTUJava;

import java.io.File;

import org.apache.log4j.Logger;
//import org.semanticweb.HermiT.Reasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.util.SimpleIRIMapper;

import at.tuvienna.utils.SettingsReader;

public class ReasonerInterface {
	private final static Logger logger = Logger.getLogger(ReasonerInterface.class);
	private OWLOntologyManager manager;
	private OWLOntology ont;
	private PelletReasoner reasoner;
	
	/*
	public OWLDataFactory createDataFactoryJenaReasoner() throws OWLOntologyCreationException,
    OWLOntologyStorageException {
		return this.createDataFactoryInternal(false);
	}*/
	
	public OWLDataFactory createDataFactory() throws OWLOntologyCreationException,
    OWLOntologyStorageException {
		return this.createDataFactoryInternal(false);
	}
		
	private OWLDataFactory createDataFactoryInternal(boolean isJenaReasoner) throws OWLOntologyCreationException,
    OWLOntologyStorageException {
		manager = OWLManager.createOWLOntologyManager();
		/*
		String filePath = Util.getFilePathWithoutFileName(SettingsReader.readProperty("output"));
		String filePathReverse = new StringBuffer(filePath).reverse().toString();
		int slashPos = filePathReverse.length() - filePathReverse.indexOf("/"); //last slash
		filePath = filePath.substring(0,slashPos);
		*/
		File pmbokOnt = new File(UMLtuOWLLoader.ONTOLOGY_URI);//SettingsReader.readProperty("output"));
		File extendedOnt = new File(UMLtuOWLLoader.ONTOLOGY_URI_INDIVIDUALS);
		manager.addIRIMapper(new SimpleIRIMapper(IRI.create(UMLtuOWLLoader.PMBOK_ONTOLOGY_IRI), IRI.create(pmbokOnt)));
		manager.addIRIMapper(new SimpleIRIMapper(IRI.create(UMLtuOWLLoader.DOMAIN_ONTOLOGY_IRI), IRI.create(extendedOnt))); //TODO comment?
		ont = manager.loadOntologyFromOntologyDocument(pmbokOnt);
		ont = manager.loadOntologyFromOntologyDocument(extendedOnt); //TODO comment?
		reasoner = this.createReasoner();
        logger.trace("VerifyQuestion.test() " + ont.getAxiomCount());
        logger.trace("VerifyQuestion.test() " + ont.getImportsClosure());
		//IRI documentIRI = manager.getOntologyDocumentIRI(ont);
		OWLDataFactory dataFactory = manager.getOWLDataFactory();
		return dataFactory;
	}
	
	public void inferAndSave() throws OWLOntologyStorageException {
		 //reasoner test
       //Reasoner reasoner=new Reasoner(ont); //HermIT
       PelletReasoner reasoner = createReasoner();
       logger.trace("Done creating reasoner.");
       //org.mindswap.pellet.KnowledgeBase cannot be resolved. It is indirectly referenced from required .class files
       reasoner.getKB().realize();
       logger.trace("Realized knowledge base.");
       if(logger.isTraceEnabled()) {
    	   reasoner.getKB().printClassTree();
       }
       
       boolean consistent = reasoner.isConsistent();
       if(!consistent) {
          logger.error("The ontology is unsatisfiable: " + reasoner.getUnsatisfiableClasses());
           throw new OWLOntologyStorageException("The ontology is unsatisfiable.");
       } else {
           logger.trace("There are no unsatisfiable classes so the Ontology will be saved.");
           //manager.saveOntology(ont, IRI.create("file:"+SettingsReader.readProperty("output")));
           manager.saveOntology(ont, IRI.create("file:"+UMLtuOWLLoader.ONTOLOGY_URI_INDIVIDUALS)); //TODO
           
       } 
	}
	
	public String getPath() {
		return SettingsReader.readProperty("output");
	}
	
	public OWLOntology getOntology() {
		return ont;
	}
	
	public OWLOntologyManager getManager() {
		return manager;
	}
	
	public String getIRI() {
		return UMLtuOWLLoader.PMBOK_ONTOLOGY_IRI;
	}
	
	public PelletReasoner createReasoner() throws OWLOntologyStorageException {
		/* causes that the reasoner only is executed the very first time 
		  if(reasoner!=null) {
			return reasoner;
		}
		*/
		try {
			reasoner = PelletReasonerFactory.getInstance().createReasoner(ont);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new OWLOntologyStorageException(e.getMessage());
		} 
		return reasoner;
	}

	public PelletReasoner createNonBufferingReasoner() throws OWLOntologyStorageException {
		/* causes that the reasoner only is executed the very first time 
		  if(reasoner!=null) {
			return reasoner;
		}
		*/
		try {
			reasoner = PelletReasonerFactory.getInstance().createNonBufferingReasoner(ont);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new OWLOntologyStorageException(e.getMessage());
		} 
		return reasoner;
	}
	
}
