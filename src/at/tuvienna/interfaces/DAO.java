package at.tuvienna.interfaces;

import java.util.List;

import at.tuvienna.umlTUJava.UMLtuException;

public interface DAO<T,ID> {
	public void insert(T t) throws UMLtuException;
	public void update(T t) throws UMLtuException;
	public void remove(T t) throws UMLtuException;
	public T readRecord(ID id) throws UMLtuException;
	public List<T> readFulltext(String s) throws UMLtuException;
}
