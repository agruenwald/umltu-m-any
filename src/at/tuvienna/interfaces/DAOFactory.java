package at.tuvienna.interfaces;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;

import at.tuvienna.umlTUJava.UMLtuException;

public class DAOFactory {
	private final static Logger logger = Logger.getLogger(DAOFactory.class);
	private final static String PKG_NAME = "itpm.dao.";
	
	
	public static DAO create(Class<?> beanClass) throws UMLtuException {
		Class<DAO> daoClass = null;
		try {
			daoClass = (Class<DAO>) Class.forName(PKG_NAME+"DAO"+beanClass.getSimpleName());
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage());
		}
		
		DAO dao = null;
		Constructor constructor = null;
		for (Constructor c : daoClass.getConstructors()) {
			if(c.getParameterTypes().length==0) {
				System.out.println("FOUND!");
				constructor = c;
			}
		}
		try {
			dao = (DAO)constructor.newInstance();
		} catch (InstantiationException e) {
			logger.error(e.getMessage());
			throw new UMLtuException(e.getMessage());
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage());
			throw new UMLtuException(e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
			throw new UMLtuException(e.getMessage());
		} catch (InvocationTargetException e) {
			logger.error(e.getMessage());
			throw new UMLtuException(e.getMessage());
		}
		return dao;
	}
	
	
	public static Object createBean(Class<?> beanClass) throws UMLtuException {
		
		Object bean = null;
		Constructor constructor = null;
		for (Constructor c : beanClass.getConstructors()) {
			if(c.getParameterTypes().length==0) {
				System.out.println("FOUND!");
				constructor = c;
			}
		}
		try {
			bean = constructor.newInstance();
		} catch (InstantiationException e) {
			logger.error(e.getMessage());
			throw new UMLtuException(e.getMessage());
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage());
			throw new UMLtuException(e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
			throw new UMLtuException(e.getMessage());
		} catch (InvocationTargetException e) {
			logger.error(e.getMessage());
			throw new UMLtuException(e.getMessage());
		}
		return bean;
	}
	
}
