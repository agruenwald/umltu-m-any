package at.tuvienna.interfaces;

import at.tuvienna.umlTUJava.UMLtuException;

public interface DAOListener<T,ID> {
	public void beforeInsert(T t) throws UMLtuException;
	public void afterInsert(T t) throws UMLtuException;
	public void beforeInsertAttributes(T t) throws UMLtuException;
	public void afterInsertAttributes(T t) throws UMLtuException; //TODO e.g. persist timesheet with attributes before inserting collaborator - how?
	public void beforeInsertAssociations(T t) throws UMLtuException;
	public void afterInsertAssociations(T t) throws UMLtuException;
	public void checkAttributes(T t) throws UMLtuException;
	public void checkAssociations(T t) throws UMLtuException;
	public void checkAttributesAdditional(T t) throws UMLtuException; //TODO 2 modes? one is override, one is extending??
	public void checkAssociationsAdditional(T t) throws UMLtuException;
	//TODO remove unnecesary rules
	//cyclic insert: two inserts: one is an update (in which however no key attribute exists) and the other one is a plain insert.
}
