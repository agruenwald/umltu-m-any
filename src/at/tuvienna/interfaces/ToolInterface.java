package at.tuvienna.interfaces;

import at.tuvienna.umlTUJava.UMLtuException;

public interface ToolInterface {
	
	/**
	 * Removes all individuals from the current ontology.
	 * @throws UMLtuException if the deletion process did not succeed
	 * (error message from native implementation received).
	 */
	public void dropAllIndividuals() throws UMLtuException;
	
	/**
	 * @return the number of individuals that are contained in the ontology, which is >= 0.
	 * @throws UMLtuException is thrown if the native implementation throws an exception.
	 */
	public Integer getOntologySize() throws UMLtuException;
}
