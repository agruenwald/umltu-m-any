package at.tuvienna.owlapi;

import java.util.Collections;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.util.OWLEntityRemover;

import at.tuvienna.interfaces.ToolInterface;
import at.tuvienna.umlTUJava.ReasonerInterface;
import at.tuvienna.umlTUJava.Transaction;
import at.tuvienna.umlTUJava.UMLtuException;

public class OwlAPITool implements ToolInterface {
	private final static Logger logger = Logger.getLogger(OwlAPITool.class);
	
	@Override
	public Integer getOntologySize() throws UMLtuException {
		logger.trace(String.format("Called getOntologySize()"));
		Integer size = 0;
		Transaction transaction = new Transaction(true);
		transaction.autoBegin();
		ReasonerInterface reasonerInterface = transaction.getReasoner();
		
		Iterator<OWLNamedIndividual> it = reasonerInterface.getOntology().getIndividualsInSignature().iterator();
		while(it.hasNext()) {
			it.next();
			size++;
		}
        transaction.autoCommit();
        return size;
		
	}
	
	
	@Override
	public void dropAllIndividuals() throws UMLtuException {
		logger.warn(String.format("Remove all Individuals from OWLAPI!"));
		Transaction transaction = new Transaction(true);
		transaction.autoBegin();
		ReasonerInterface reasonerInterface = transaction.getReasoner();
		
		OWLEntityRemover remover = new OWLEntityRemover(reasonerInterface.getManager(), Collections.singleton(reasonerInterface.getOntology()));
		Iterator<OWLNamedIndividual> it = reasonerInterface.getOntology().getIndividualsInSignature().iterator();
		while(it.hasNext()) {
			OWLNamedIndividual ind = (it.next());
			ind.accept(remover);
			logger.info(String.format("Remove %s.",ind));
		}
        reasonerInterface.getManager().applyChanges(remover.getChanges());
        transaction.autoCommit();
	}

}
