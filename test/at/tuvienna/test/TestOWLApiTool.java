package at.tuvienna.test;

import static org.junit.Assert.*;
import org.junit.Test;

import at.tuvienna.owlapi.OwlAPITool;
import at.tuvienna.umlTUJava.UMLtuException;

public class TestOWLApiTool {

	@Test
	public void testDropAllIndividuals() throws UMLtuException {
		OwlAPITool owlAPITool = new OwlAPITool();
		owlAPITool.dropAllIndividuals();
		assertTrue(owlAPITool.getOntologySize() == 0);
	}

}
