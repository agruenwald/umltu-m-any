Transformation of UML diagrams (Visual Paradigm, Visio and others) into any piece of code, 
including access patterns (DAOs) and automatically generated unit
tests!

If you have problems with the document encodings of the xtend generators, then
please use UTF-8 format! Regex-command for replacing wrong encoding automatically: 
Replace �([^�]+)� by «$1» (should replace at least most of the wrong characters).
